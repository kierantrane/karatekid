Feature: Print hello world feature

Scenario: Hello world scenario

	* print '*********************  hello world'
	* print '*********************  hello kieran, do you know karate?'
	
Scenario: declare and print variables

	* def balance = 200
	* def fee = 20
	* def tax = 10
	* print ' ***********************  total amount -> '+(balance+fee+tax)
	
	* print ' ******************* Multiply total amount -> '+(balance*fee*tax)
Feature: GET API Feature

  Scenario: Get user details
    Given url baseUrl+'/public/v1/users'
    And path '37'
    When method GET
    Then status 200
    
    * print response
    * def jsonResponse = response
    * print jsonResponse
    * def actName = jsonResponse.data.name
    * print "Test print= "+actName
    * match actName == 'Lambda'
    * def actId = jsonResponse.data.id
    * def actEmail = jsonResponse.data.email
    * def actGender = jsonResponse.data.gender
    * print myVarName
    #Validation here
    * match actId == 37
    * match actEmail == 'lam123@gmail.com'
    * match actGender == 'male'

  Scenario: Get user details - user not found
    Given url baseUrl+'/public/v1/users'
    And path '1'
    When method GET
    Then status 404

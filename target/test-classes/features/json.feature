Feature: json feature testing


Scenario: json reader parser
	* def jsonObject = 
	"""
	[
	{
		"name" : "Tom",
		"city" : "Bangalore",
		"age" : 25
	},
	{
		"name" : "Anna",
		"city" : "limerick",
		"age" : 29
	}
	]
	"""
	
	* print jsonObject
	* print jsonObject[0].name
	* print jsonObject[1].name +"," +jsonObject[1].city+","+jsonObject[1].age
	
	Scenario: complex json reader
	
 	* def jsonObject2 = 
"""
{
"menu": {
  "id": "file",
  "value": "File",
  "popup": {
    "menuitem": [
      {"value": "New", "onclick": "CreateNewDoc()"},
      {"value": "Open", "onclick": "OpenDoc()"},
      {"value": "Close", "onclick": "CloseDoc()"}
    ]
  }
}
}
"""
 * print jsonObject2
 
 * print jsonObject2.menu
 * print jsonObject2.menu.id
  * print jsonObject2.menu.popup.menuitem[0].value
  * print jsonObject2.menu.popup.menuitem[0].onclick
  * print jsonObject2.menu.popup.menuitem[1].value
  * print jsonObject2.menu.popup.menuitem[1].onclick






	
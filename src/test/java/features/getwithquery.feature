Feature: GET API with Query Parameters

Scenario: Get all active users, female and by Id
* def query = {status: 'active', gender: 'female', id :81}
Given url baseUrl+'/public/v1/users'
And params query 
When method GET
Then status 200 
* print response

Scenario: Get all active users and get the count
* def query = {status: 'active'}
Given url baseUrl+'/public/v1/users'
And params query
When method GET
Then status 200 
* def jsonResponse = response
* print jsonResponse

* def userCount = jsonResponse.data.length
* match userCount == 20
* print userCount
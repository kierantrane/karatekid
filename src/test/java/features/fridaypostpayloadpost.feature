Feature: Create user with random data using a post api

Background: 
	* url 'https://gorest.co.in'
	
	#Function to create random 10 digit string
	* def random_string =
	"""
	function(s){
	var text = "";
	var pattern = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	for(var i = 0; i<s; i++)
		text += pattern.charAt(Math.floor(Math.random() * pattern.length()));
		return text;
	}
	"""
	#End function
	
	# Call function and assign to variable
	* def randomString = random_string(10)
	* def randomName = random_string(6)
	
	# Create random email
	* def emailCreated = randomString+"@1email.com"
	
	 #Output is
	 * print "random ="+randomString
	 * print "emailCreated ="+emailCreated
	  
	# Standard payload here
	* def requestPayLoad = 
	"""
		{
            "name": "Kieran",
            "email": "emai3l@jj.com",
            "gender": "male",
            "status": "active"
		}
	"""
	# Update the values in the Json here
	* requestPayLoad.email = emailCreated
	* requestPayLoad.name =randomName
	
	#Print updated Json to be posted to the endpoint
	* print requestPayLoad
	
	Scenario: Create a user with the given data
	Given path  '/public/v1/users'
	
	#Json file above
	And request requestPayLoad  
	#Submit with Auth bearer token
	And header Authorization = 'Bearer ' +tokenID
	
	# Submit Json
	When method post  
	Then status 201
	
	#Print response
	* print response
	
	#Getting the response as a Json
	* def jsonResponse = response
	* print jsonResponse

#Getting the data from the Json Object
* def actId = jsonResponse.data.id
* def actName = jsonResponse.data.name
* def actGender = jsonResponse.data.gender
* def actStatus = jsonResponse.data.status
* def actEmail = jsonResponse.data.email

#Getting the field data from the Json Object
* print "Test print actName= "+actName
* print "Test print actId= "+actId
* print "Test print actGender= "+actGender
* print "Test print actStatus= "+actStatus
* print "Test print actEmail= "+actEmail

#	Assertions on data this way
	And match actName == '#present'
	And match actStatus == '#present'
	And match actEmail == '#present'
	And match actId == '#present'
	And match actGender == '#present'
	
	# Or assert the data this way
		And match $.data.id == '#present'
		And match $.data.name == '#present'
		And match $.data.gender == '#present'
		And match $.data.status == '#present'
		And match $.data.email == '#present'
	
	# Or this
	#		And match $.data.name == 'Kieran'
			And match $.data.name !contains 'Kieran'
	
	
#Static email, this needs to be updated every time here

Feature: Create user using a post api

Background: 
	* url 'https://gorest.co.in' 
	* def requestLoad = 
	"""
	{
            "name": "Kieran",
            "email": "xRH2288eAS@1email.com",
            "gender": "male",
            "status": "active"
}
	"""
	
	Scenario: Create a user with the given data
	Given path  '/public/v1/users'
	And request requestLoad
	And header Authorization = 'Bearer ' +tokenID
	When method post
	Then status 201
	* print response
	
	#Getting the response as a Json
	* def jsonResponse = response
	* print jsonResponse

#Getting the data from the Json Object
* def actId = jsonResponse.data.id
* def actName = jsonResponse.data.name
* def actGender = jsonResponse.data.gender
* def actStatus = jsonResponse.data.status
* def actEmail = jsonResponse.data.email

#Getting the field data from the Json Object
* print "Test print actName= "+actName
* print "Test print actId= "+actId
* print "Test print actGender= "+actGender
* print "Test print actStatus= "+actStatus
* print "Test print actEmail= "+actEmail

#	Assertions on data this way
	And match actName == '#present'
	And match actStatus == '#present'
	And match actEmail == '#present'
	And match actId == '#present'
	And match actGender == '#present'
	
	# Or assert the data this way
		And match $.data.id == '#present'
		And match $.data.name == '#present'
		And match $.data.gender == '#present'
		And match $.data.status == '#present'
	And match $.data.email == '#present'
	
	# Or this
			And match $.data.name == 'Kieran'
	
	
	
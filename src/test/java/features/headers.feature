Feature: GET API headers feature

Scenario: Pass request with headers_Part1
And header Accept-Encoding = 'gzip, deflate, br'
And header Vary = 'Accept-Encoding'
And header Connection = 'Keep-Alive'

Given url baseUrl+'/public/v1/users'
And path '2091'
When method GET
Then status 200 
* print response


Scenario: Pass request with headers_Part2
* def request_headers = {Accept-Encoding : 'gzip, deflate, br', Vary : 'Accept-Encoding', Connection : 'Keep-Alive'}
Given headers request_headers
Given url baseUrl+'/public/v1/users'
And path '2091'
When method GET
Then status 200 
* print response

Scenario: Pass request with headers_Part3
* configure headers = {Accept-Encoding : 'gzip, deflate, br', Vary : 'Accept-Encoding', Connection : 'Keep-Alive'}
Given url baseUrl+'/public/v1/users'
And path '2091'
When method GET
Then status 200 
* print response